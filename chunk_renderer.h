//
// Created by sbg on 7/29/17.
//

#ifndef LONGSHOT_CHUNK_RENDERER_H
#define LONGSHOT_CHUNK_RENDERER_H

#include <GLES2/gl2.h>
#include <vector>

#include "chunk.h"
#include "gles_renderable.h"

class gles_material;

class chunk_renderer : public gles_renderable {
 public:
  chunk_renderer(const chunk &chunk, const gles_material &material, const gles_material &line_material);

  virtual void render(const glm::mat4 &current_model_transfrom, const glm::mat4 &current_view_transfrom, const glm::mat4 &current_projection_transfrom, const glm::ivec3 &center_chunk);

 private:
  static constexpr size_t edges = chunk::size + 1;
  //void generate_index_buffer();
  //void generate_local_vertex_buffer();
  //static void generate_global_vertex_buffer();
  void generate_buffers();

  void add_face_x_positive(size_t x, size_t y, size_t z, block_t material);
  void add_face_x_negative(size_t x, size_t y, size_t z, block_t material);
  void add_face_y_positive(size_t x, size_t y, size_t z, block_t material);
  void add_face_y_negative(size_t x, size_t y, size_t z, block_t material);
  void add_face_z_positive(size_t x, size_t y, size_t z, block_t material);
  void add_face_z_negative(size_t x, size_t y, size_t z, block_t material);

  enum class direction {
    east = 0, // X postive
    west = 1, // X negative
    north = 2, // Z negative
    south = 3, // Z positive
    up = 4, // Y positve
    down = 5 // Y negative
  };

  struct local_vertex_data {
    GLfloat x, y, z, w;
    GLfloat s, t;
    GLfloat nx, ny, nz, nw;
    GLuint material;

    static const size_t offset_vertex_coords;
    static const size_t offset_texture_coords;
    static const size_t offset_normal_coords;
    static const size_t offset_material;
  };

  struct line_vertex_data {
    GLfloat x, y, z, w;
    GLfloat r, g, b, a;

    static const size_t offset_vertex_coords;
    static const size_t offset_vertex_colours;
  };

  const gles_material &material;
  const gles_material &line_material;

  std::vector<local_vertex_data> vertex_buffer_data;
  GLuint vertex_buffer;

  std::vector<line_vertex_data> line_buffer_data;
  GLuint line_buffer;

  const chunk &chunk;
  size_t chunk_version;
};

#endif //LONGSHOT_CHUNK_RENDERER_H
