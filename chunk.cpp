//
// Created by sbg on 7/29/17.
//

#include "chunk.h"

#include <glm/vec3.hpp>
#include <glm/gtc/noise.hpp>
#include <set>
#include <queue>
#include <unordered_set>
#include <iostream>

#include "hashes.h"

chunk::chunk(int x, int y, int z) : x(x), y(y), z(z), version(0) {
  for(size_t x = 0; x < size; ++x) {
    for(size_t y = 0; y < size; ++y) {
      for(size_t z = 0; z < size; ++z) {
        set(0, x, y, z);
      }
    }
  }
}

struct biome {
  float height_scale, height_offset;
  float noise_scale, noise_effect;
  float grass_depth;
};

void chunk::generate() {
  constexpr int offset = 1024;

  // biome b { 32, 8, 0.1f, 0.1f, 4 }; // Baseline
  // biome b { 4, 8, 0.05f, 0.5f, 2 }; // Desert / Rough plains
  // biome b { 3, 0, 0.01f, 1.f, 4 }; // Plains
  // biome b { 24, 0, 0.03f, 0.5f, 4 }; // Rolling hills
  biome b { 48, 0, 0.03f, 0.1f, 2 }; // Mountainous
  // biome b { 0, 0, 0.0f, 1.0f, 2 }; // Flat world


  for(size_t x = 0; x < size; ++x) {
    for(size_t z = 0; z < size; ++z) {
      auto height = glm::simplex(glm::vec2(x + this->x * size + offset, z + this->z * size + offset) * 0.01f) * b.height_scale + b.height_offset;
      height -= this->y * size;

      for(size_t y = 0; y < size; ++y) {
        auto cutoff = (y - height) * b.noise_effect;
        auto noise = glm::simplex(glm::vec3(x + this->x * size + offset, y + this->y * size + offset, z + this->z * size + offset) * b.noise_scale);
        if(noise > cutoff) {
          if(y < height - b.grass_depth) {
            set(1, x, y, z);
          } else {
            set(2, x, y, z);
          }
        } else {
          set(0, x, y, z);
        }
      }
    }
  }

  std::unordered_set<glm::ivec3> seen;
  for(size_t x = 0; x < size; ++x) {
    for(size_t z = 0; z < size; ++z) {
      for(size_t y = 0; y < size; ++y) {
        kill_floater(seen, x, y, z);
      }
    }
  }

  ++version;
}

block_t chunk::get(size_t x, size_t y, size_t z) const {
  size_t index = (x * size + y) * size + z;
  return blocks[index];
}


glm::vec3 chunk::get_relative_position(const glm::ivec3 &center_chunk) const {
  return glm::vec3((this->x - center_chunk.x) * size, (this->y - center_chunk.y) * size, (this->z - center_chunk.z) * size);
}

void chunk::set(block_t block, size_t x, size_t y, size_t z) {
  size_t index = (x * size + y) * size + z;
  blocks[index] = block;
}

void chunk::kill_floater(std::unordered_set<glm::ivec3> &seen, size_t x, size_t y, size_t z) {
  constexpr size_t floater_limit = 128;

  if(get(x, y, z) == 0) {
    return;
  }

  if(seen.count({x, y, z}) != 0) {
    return;
  }

  std::unordered_set<glm::ivec3> blocks;
  std::queue<glm::ivec3> open;
  open.push({x, y, z});

  while(!open.empty() && blocks.size() < floater_limit) {
    auto cur = open.front();
    auto x = cur.x;
    auto y = cur.y;
    auto z = cur.z;

    blocks.insert(cur);
    seen.insert(cur);

    if(cur.x == 0) {
      break;
    } else {
      if(get(x - 1, y, z) != 0) {
        if(blocks.count({x - 1, y, z}) == 0) {
          open.push({x - 1, y, z});
        }
      }
    }

    if(cur.x == size - 1) {
      break;
    } else {
      if(get(x + 1, y, z) != 0) {
        if (blocks.count({x + 1, y, z})==0) {
          open.push({x + 1, y, z});
        }
      }
    }

    if(cur.y == 0) {
      break;
    } else {
      if(get(x, y - 1, z) != 0) {
        if(blocks.count({x, y - 1, z}) == 0) {
          open.push({x, y - 1, z});
        }
      }
    }

    if(cur.y == size - 1) {
      break;
    } else {
      if(get(x, y + 1, z) != 0) {
        if(blocks.count({x, y + 1, z}) == 0) {
          open.push({x, y + 1, z});
        }
      }
    }

    if(cur.z == 0) {
      break;
    } else {
      if(get(x, y, z - 1) != 0) {
        if(blocks.count({x, y, z - 1}) == 0) {
          open.push({x, y, z - 1});
        }
      }
    }

    if(cur.z == size - 1) {
      break;
    } else {
      if(get(x, y, z + 1) != 0) {
        if(blocks.count({x, y, z + 1}) == 0) {
          open.push({x, y, z + 1});
        }
      }
    }

    open.pop();

    //std::cout << "blocks: " << blocks.size() << std::endl;
    //std::cout << "open: " << open.size() << std::endl;
  }

  //std::cout << "done!" << std::endl;
  //std::cout << "blocks: " << blocks.size() << std::endl;
  //std::cout << "open: " << open.size() << std::endl;

  if(open.empty()) {
    for(auto block : blocks) {
      set(0, block.x, block.y, block.z);
    }
  }

  /*
  if(get(x, y, z) != 0 &&
      (x == 0 || get(x - 1, y, z) == 0) &&
      (x == size - 1 || get(x + 1, y, z) == 0) &&
      (y == 0 || get(x, y - 1, z) == 0) &&
      (y == size - 1 || get(x, y + 1, z) == 0) &&
      (z == 0 || get(x, y, z - 1) == 0) &&
      (z == size - 1 || get(x, y, z + 1) == 0)) {
    set(0, x, y, z);
  }
  */
}
