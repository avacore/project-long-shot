//
// Created by sbg on 8/26/17.
//

#include <cmath>
#include <iostream>
#include "world.h"

world::world() {
  for(int x = -radius; x <= radius; ++x) {
    for(int y = -radius; y <= radius; ++y) {
      for(int z = -radius; z <= radius; ++z) {
        if(x*x + y*y + z*z <= radius_sqr) {
          view_sphere.emplace(x, y, z);
        }
      }
    }
  }

  chunks.reserve((radius * 2 + 1) * (radius * 2 + 1) * (radius * 2 + 1));
}

const std::vector<std::unique_ptr<chunk>>& world::get_close_chucks(const absolute_position &center) {
  std::unordered_set<glm::ivec3> render_chunks;

  for(auto const &chunk_position : view_sphere) {
    render_chunks.emplace(chunk_position.x + center.get_chunk_postion().x, chunk_position.y + center.get_chunk_postion().y, chunk_position.z + center.get_chunk_postion().z);
  }

  /*chunks.erase(std::remove_if(chunks.begin(), chunks.end(), [&render_chunks](const std::unique_ptr<chunk> &chunk) {
    return render_chunks.count(chunk->get_chunk_position()) < 1;
  }), chunks.end());*/

  for(auto iter = chunks.begin(); iter != chunks.end(); ) {
    if(render_chunks.count((*iter)->get_chunk_position()) > 0) {
      ++iter;
    } else {
      for(auto &listener : chunk_listeners) {
        listener->chunk_removed(**iter);
      }
      chunks.erase(iter);
    }
  }

  for(auto const &chunk : chunks) {
    if(render_chunks.count(chunk->get_chunk_position()) > 0) {
      render_chunks.erase(chunk->get_chunk_position());
    }
  }

  if(render_chunks.size() > 0) {
    //std::cout << "Loaded chunks: " << chunks.size() << std::endl;
    //std::cout << "Missing chunks: " << render_chunks.size() << std::endl;

    glm::ivec3 chunk_position;
    int dist = std::numeric_limits<int>::max();
    for(auto position : render_chunks) {
      int cand_dist = std::abs(position.x - center.get_chunk_postion().x) + std::abs(position.y - center.get_chunk_postion().y) + std::abs(position.z - center.get_chunk_postion().z);
      if(cand_dist < dist) {
        dist = cand_dist;
        chunk_position = position;
      }
    }

    //std::cout << "New chunk: " << chunk_position.x << ", " << chunk_position.y << ", " << chunk_position.z << std::endl;
    chunks.push_back(std::move(std::make_unique<chunk>(chunk_position.x, chunk_position.y, chunk_position.z)));
    auto &chunk = **chunks.rbegin();
    chunk.generate();
    for(auto &listener : chunk_listeners) {
      listener->chunk_added(chunk);
    }
  }

  return chunks;
}

void world::chunk_listener_add(chunk_listener *listener) {
  chunk_listeners.push_back(listener);
}