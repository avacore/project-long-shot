//
// Created by sbg on 8/26/17.
//

#ifndef LONGSHOT_ABSOLUTE_POSITION_H
#define LONGSHOT_ABSOLUTE_POSITION_H

#include <glm/vec3.hpp>
#include "chunk.h"

class absolute_position {
 public:
  absolute_position();

  void operator+=(glm::vec3 offset);

  inline glm::vec3 get_local_postion() const { return glm::vec3(x, y, z); }
  inline glm::ivec3 get_chunk_postion() const { return glm::ivec3(cx, cy, cz); }

 private:
  static constexpr int bound = chunk::size / 2;
  static constexpr int step = chunk::size;

  void normalise();

  int cx, cy, cz;
  float x, y, z;
};

#endif //LONGSHOT_ABSOLUTE_POSITION_H
