//
// Created by sbg on 7/29/17.
//

#ifndef LONGSHOT_GLES_RENDERABLE_H
#define LONGSHOT_GLES_RENDERABLE_H

#include <GLES2/gl2.h>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

class gles_renderable {
 public:
  virtual ~gles_renderable() = default;

  virtual void render(const glm::mat4 &current_model_transfrom, const glm::mat4 &current_view_transfrom, const glm::mat4 &current_projection_transfrom, const glm::ivec3 &center_chunk) = 0;
};


#endif //LONGSHOT_GLES_RENDERABLE_H
