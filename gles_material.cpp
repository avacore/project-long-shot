//
// Created by sbg on 8/5/17.
//

#include <fstream>
#include <iostream>
#include "gles_material.h"

#include "yaml-cpp/yaml.h"

static std::string readFile(const std::string &name) {
  std::fstream stream(name, std::ios::in);

  std::istreambuf_iterator<char> begin(stream), end;

  return std::string(begin, end);
};

gles_material::gles_material(std::string name) : uniform_model_transfrom(-1), uniform_view_transfrom(-1), uniform_projection_transfrom(-1), uniform_daylight_direction(-1), attribute_vertex_position(-1), attribute_vertex_normal(-1), attribute_vertex_texture(-1), attribute_vertex_colour(-1), attribute_material(-1) {
  std::string file = "materials/" + name + ".yaml";

  YAML::Node description = YAML::LoadFile(file);

  this->shader_vertex = load_shader(GL_VERTEX_SHADER, description["shaders"]["vertex"].as<std::string>());
  this->shader_fragment = load_shader(GL_FRAGMENT_SHADER, description["shaders"]["fragment"].as<std::string>());

  this->shader_program = glCreateProgram();
  glAttachShader(this->shader_program, this->shader_vertex);
  glAttachShader(this->shader_program, this->shader_fragment);
  glLinkProgram(this->shader_program);

  char buffer[65536];
  GLsizei length;
  glGetProgramInfoLog(this->shader_program, 65536, &length, buffer);
  std::cout << "Shader link info:\n" << std::string(buffer, length) << std::endl;

  int texture_unit = 0;
  for(auto texture : description["textures"]) {
    std::cout << "Texture name: " << texture.first << std::endl;
    std::cout << "  Is map: " << texture.second.IsMap() << std::endl;
    std::cout << "  Is array: " << texture.second.IsSequence() << std::endl;
    auto name = texture.first.as<std::string>();

    GLuint texture_handle;
    GLenum texture_type;
    if(texture.second.IsMap()) {
      auto top = texture.second["top"].as<std::string>();
      auto bottom = texture.second["bottom"].as<std::string>();
      auto front = texture.second["front"].as<std::string>();
      auto back = texture.second["back"].as<std::string>();
      auto left = texture.second["left"].as<std::string>();
      auto right = texture.second["right"].as<std::string>();

      texture_handle = load_texture_cubemap(top, bottom, front, back, left, right);
      texture_type = GL_TEXTURE_CUBE_MAP;
    } else if(texture.second.IsSequence()) {
      std::vector<std::string> files;

      for(auto file : texture.second) {
        files.push_back(file.as<std::string>());
      }

      texture_handle = load_texture_2d_array(files);
      texture_type = GL_TEXTURE_2D_ARRAY;
    } else {
      auto file = texture.second.as<std::string>();
      texture_handle = load_texture_2d(file);
      texture_type = GL_TEXTURE_2D;
    }

    GLint sampler_loc = glGetUniformLocation(this->shader_program, name.c_str());
    textures.emplace(name, std::make_tuple(texture_handle, texture_unit, sampler_loc, texture_type, nullptr));
    ++texture_unit;
  }
}

void gles_material::activate() const {
  glUseProgram(this->shader_program);

  for(auto& [name, texture] : textures) {
    auto& [texture_handle, texture_unit, sampler_loc, texture_type, texture_data] = texture;

    glActiveTexture(GL_TEXTURE0 + texture_unit);
    glBindTexture(texture_type, texture_handle);
    glUniform1i(sampler_loc, texture_unit);
  }
}

GLint gles_material::get_uniform_model_transfrom() const {
  if(uniform_model_transfrom == -1) {
    uniform_model_transfrom = glGetUniformLocation(this->shader_program, "model_transform");
  }
  return uniform_model_transfrom;
}

GLint gles_material::get_uniform_view_transfrom() const {
  if(uniform_view_transfrom == -1) {
    uniform_view_transfrom = glGetUniformLocation(this->shader_program, "view_transform");
  }
  return uniform_view_transfrom;
}

GLint gles_material::get_uniform_projection_transfrom() const {
  if(uniform_projection_transfrom == -1) {
    uniform_projection_transfrom = glGetUniformLocation(this->shader_program, "projection_transform");
  }
  return uniform_projection_transfrom;
}

GLint gles_material::get_uniform_daylight_direction() const {
  if(uniform_daylight_direction == -1) {
    uniform_daylight_direction = glGetUniformLocation(this->shader_program, "daylight_direction");
  }
  return uniform_daylight_direction;
}

GLint gles_material::get_attribute_vertex_normal() const {
  if(attribute_vertex_normal == -1) {
    attribute_vertex_normal = glGetAttribLocation(this->shader_program, "normal");
  }
  return attribute_vertex_normal;
}

GLint gles_material::get_attribute_vertex_position() const {
  if(attribute_vertex_position == -1) {
    attribute_vertex_position = glGetAttribLocation(this->shader_program, "vertex");
  }
  return attribute_vertex_position;
}

GLint gles_material::get_attribute_vertex_texture() const {
  if(attribute_vertex_texture == -1) {
    attribute_vertex_texture = glGetAttribLocation(this->shader_program, "texture");
  }
  return attribute_vertex_texture;
}

GLint gles_material::get_attribute_vertex_colour() const {
  if(attribute_vertex_colour == -1) {
    attribute_vertex_colour = glGetAttribLocation(this->shader_program, "colour");
  }
  return attribute_vertex_colour;
}

GLint gles_material::get_attribute_material() const {
  if(attribute_material == -1) {
    attribute_material = glGetAttribLocation(this->shader_program, "material");
  }
  return attribute_material;
}

GLuint gles_material::load_shader(GLenum type, const std::string &file) {
  GLuint shader = glCreateShader(type);
  std::string shader_src = readFile(file);
  const char *shader_src_ptr = shader_src.c_str();
  glShaderSource(shader, 1, &shader_src_ptr, nullptr);
  glCompileShader(shader);

  char buffer[65536];
  GLsizei length;
  glGetShaderInfoLog(shader, 65536, &length, buffer);
  std::cout << "Shader compile info:\n" << std::string(buffer, length) << std::endl;

  return shader;
}

GLuint gles_material::load_texture_2d(const std::string &file) {
  auto texture_data = std::make_unique<class texture>(file);

  GLuint texture_handle;
  glGenTextures(1, &texture_handle);
  glBindTexture(GL_TEXTURE_2D, texture_handle);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture_data->get_width(), texture_data->get_height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_data->get_data());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  return texture_handle;
}

GLuint gles_material::load_texture_2d_array(const std::vector<std::string> &files) {
  GLuint texture_handle = -1;
  size_t width, height;
  GLint depth = 0;

  for(auto &file : files) {
    texture texture_data(file);

    if(texture_handle == -1) {
      glGenTextures(1, &texture_handle);
      glBindTexture(GL_TEXTURE_2D_ARRAY, texture_handle);

      glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA8, texture_data.get_width(), texture_data.get_height(), files.size());
      width = texture_data.get_width();
      height = texture_data.get_height();
    } else {
      assert(texture_data.get_width() == width);
      assert(texture_data.get_height() == height);
    }

    glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, depth, width, height, 1, GL_RGBA, GL_UNSIGNED_BYTE, texture_data.get_data());
    ++depth;
  }

  glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  return texture_handle;
}

GLuint gles_material::load_texture_cubemap(const std::string &top, const std::string &bottom, const std::string &front, const std::string &back, const std::string &left, const std::string &right) {

  GLuint texture_handle;
  glGenTextures(1, &texture_handle);
  glBindTexture(GL_TEXTURE_CUBE_MAP, texture_handle);

  {
    texture texture_data(top);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA, texture_data.get_width(), texture_data.get_height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_data.get_data());
  }

  {
    texture texture_data(bottom);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA, texture_data.get_width(), texture_data.get_height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_data.get_data());
  }

  {
    texture texture_data(front);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA, texture_data.get_width(), texture_data.get_height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_data.get_data());
  }

  {
    texture texture_data(back);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA, texture_data.get_width(), texture_data.get_height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_data.get_data());
  }

  {
    texture texture_data(left);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA, texture_data.get_width(), texture_data.get_height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_data.get_data());
  }

  {
    texture texture_data(right);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA, texture_data.get_width(), texture_data.get_height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_data.get_data());
  }

  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  return texture_handle;
}