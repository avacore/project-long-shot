//
// Created by sbg on 9/9/17.
//

#include "skybox_renderer.h"

#include <glm/gtc/type_ptr.hpp>

skybox_renderer::skybox_renderer(const std::string &material) : material(material) {
  if(!buffers_inited) {
    generate_buffers();
    buffers_inited = true;
  }
}

void skybox_renderer::generate_buffers() {
  glGenBuffers(1, &vertex_buffer);
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
  glBufferData(GL_ARRAY_BUFFER, vertex_buffer_data.size() * sizeof(GLfloat), vertex_buffer_data.data(), GL_STATIC_DRAW);

  glGenBuffers(1, &index_buffer);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, index_buffer_data.size() * sizeof(GLushort), index_buffer_data.data(), GL_STATIC_DRAW);
}

void skybox_renderer::render(const glm::mat4 &current_model_transfrom, const glm::mat4 &current_view_transfrom, const glm::mat4 &current_projection_transfrom, const glm::ivec3 &center_chunk) {
  material.activate();
  glDepthFunc(GL_LEQUAL);

  glUniformMatrix4fv(material.get_uniform_view_transfrom(), 1, GL_FALSE, glm::value_ptr(current_view_transfrom));
  glUniformMatrix4fv(material.get_uniform_projection_transfrom(), 1, GL_FALSE, glm::value_ptr(current_projection_transfrom));

  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
  glVertexAttribPointer(material.get_attribute_vertex_position(), 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
  glEnableVertexAttribArray(material.get_attribute_vertex_position());

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
  glDrawElements(GL_TRIANGLES, index_buffer_data.size(), GL_UNSIGNED_SHORT, 0);
}

std::array<GLfloat, 4 * 8> skybox_renderer::vertex_buffer_data = {
     1,  1,  1, 0,
     1,  1, -1, 0,
     1, -1,  1, 0,
     1, -1, -1, 0,
    -1,  1,  1, 0,
    -1,  1, -1, 0,
    -1, -1,  1, 0,
    -1, -1, -1, 0,
};

std::array<GLushort, 3 * 12> skybox_renderer::index_buffer_data = {
    0, 1, 2,
    2, 1, 3,
    4, 6, 5,
    5, 6, 7,
    0, 4, 1,
    1, 4, 5,
    2, 3, 6,
    6, 3, 7,
    0, 2, 4,
    4, 2, 6,
    1, 5, 3,
    3, 5, 7,
};

bool skybox_renderer::buffers_inited = false;
GLuint skybox_renderer::vertex_buffer;
GLuint skybox_renderer::index_buffer;
