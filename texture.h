//
// Created by sbg on 7/29/17.
//

#ifndef LONGSHOT_TEXTURE_H
#define LONGSHOT_TEXTURE_H

#include <string>
#include <vector>

#include <glm/vec4.hpp>

class texture {
 public:
  //typedef glm::tvec4<uint8_t> colour;
  struct colour {uint8_t r, g, b, a;};

  texture(const std::string &file);

  size_t get_width() const;
  size_t get_height() const;
  const colour *get_data() const;

 private:
  size_t width, height;
  std::vector<colour> image;
};

static_assert(sizeof(texture::colour) == 4);

#endif //LONGSHOT_TEXTURE_H
