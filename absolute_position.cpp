//
// Created by sbg on 8/26/17.
//

#include "absolute_position.h"

absolute_position::absolute_position() : cx(0), cy(0), cz(0), x(0), y(0), z(0) {
}

void absolute_position::operator+=(glm::vec3 offset) {
  x += offset.x;
  y += offset.y;
  z += offset.z;

  normalise();
}

void absolute_position::normalise() {
  while(x < -bound) {
    x += step;
    --cx;
  }
  while(x > bound) {
    x -= step;
    ++cx;
  }
  while(y < -bound) {
    y += step;
    --cy;
  }
  while(y > bound) {
    y -= step;
    ++cy;
  }
  while(z < -bound) {
    z += step;
    --cz;
  }
  while(z > bound) {
    z -= step;
    ++cz;
  }

}