//
// Created by sbg on 7/29/17.
//

#include "texture.h"

#include <cassert>
#include <fstream>
#include <iostream>

texture::texture(const std::string &file) {
  std::ifstream stream(file, std::ios::in);

  std::string magic;

  stream >> magic;

  //std::cout << "Magic: " << magic << std::endl;

  assert(magic == "P3");

  //std::cout << "Next: " << stream.peek() << std::endl;

  while(std::isspace(stream.peek()) || stream.peek() == '#') {
    std::cout << "Skipping: " << stream.peek() << std::endl;
    if(stream.peek() == '#') {
      std::cout << "First" << std::endl;
      while(stream.get() != '\n') {

      }
    } else {
      //std::cout << "Second" << std::endl;
      stream.get();
    }
    //std::cout << "End" << std::endl;
  }


  int max_color;
  stream >> width >> height >> max_color;

  //std::cout << "Image: " << width << " x " << height << " @ " << max_color << std::endl;

  assert(max_color == 255);

  image.resize(width*height);

  for(size_t y = 0; y < height; ++y) {
    for(size_t x = 0; x < width; ++x) {
      int r, g, b;

      stream >> r >> g >> b;

      image[y*width + x] = {
          static_cast<uint8_t>(r),
          static_cast<uint8_t>(g),
          static_cast<uint8_t>(b),
          255
      };
    }
  }
}

size_t texture::get_width() const {
  return this->width;
}

size_t texture::get_height() const {
  return this->height;
}

const texture::colour* texture::get_data() const {
  return this->image.data();
}