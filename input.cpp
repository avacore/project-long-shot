//
// Created by sbg on 7/22/17.
//

#include "input.h"

#include <iostream>

#include <emscripten/html5.h>

namespace input {
  axis::axis(std::string name, std::vector<axis_def> defaults) : controls(defaults) {
  }

  float axis::get() {
    float value = 0;
    for(auto &control : controls) {
      float motion = 0.f;

      switch(control.type) {
        case axis_def::Mouse:
          motion = raw_input::get_instance().get_mouse_motion(control.index);
          break;
        case axis_def::Joystick:
          motion = raw_input::get_instance().get_gamepad_axis(control.index);
          break;
      }
      if(motion != 0.f) {
        return motion;
      }
    }
    return 0;
  }

  button::button(std::string name, std::vector<button_def> defaults) : keys(defaults) {
  }

  bool button::get() {
    for(auto &key : keys) {
      switch(key.type) {
        case button_def::Keyboard:
          if(raw_input::get_instance().is_pressed(key.index.name)) {
            return true;
          }
          break;
        case button_def::Joystick:
          if(raw_input::get_instance().get_gamepad_button(key.index.number)) {
            return true;
          }
          break;
      }
    }
    return false;
  }

  raw_input& raw_input::get_instance() {
    if(instance == nullptr) {
      instance = new raw_input();
    }

    return *instance;
  }

  raw_input::raw_input() : mouse_speed(0.01) {
    emscripten_set_keydown_callback(nullptr, this, true, keydown);
    emscripten_set_keyup_callback(nullptr, this, true, keyup);
    emscripten_set_mousemove_callback(nullptr, this, true, mousemove);
    emscripten_set_pointerlockchange_callback(nullptr, this, true, pointerlock_change);
    emscripten_set_gamepadconnected_callback(this, true, gamepad_connected);
    emscripten_set_gamepaddisconnected_callback(this, true, gamepad_disconnected);

    emscripten_request_pointerlock("canvas", EM_TRUE);
  }

  bool raw_input::is_pressed(const std::string &key) const {
    return this->pressed.find(key) != this->pressed.end();
  }

  glm::vec2 raw_input::get_mouse_motion() {
    glm::vec2 ret = mouse_speed * this->mouse_motion;
    this->mouse_motion = glm::vec2(0.f, 0.f);
    return ret;
  }

  float raw_input::get_mouse_motion(size_t idx) {
    float ret = mouse_speed * this->mouse_motion[idx];
    this->mouse_motion[idx] = 0;
    return ret;
  }

  float raw_input::get_gamepad_axis(size_t idx) {
    for(auto &[index, state] : gamepads_connected) {
      if(fabs(state.axis[idx]) > 0.2f) {
        return state.axis[idx];
      }
    }
    return 0.f;
  }

  bool raw_input::get_gamepad_button(size_t idx) {
    for(auto &[index, state] : gamepads_connected) {
      if(state.digitalButton[idx] == 1) {
        return true;
      }
    }
    return false;
  }

  void raw_input::tick() {
    for(auto &[index, state] : gamepads_connected) {
      emscripten_get_gamepad_status(index, &state);
    }
  }

  EM_BOOL raw_input::keydown(int eventType, const EmscriptenKeyboardEvent *evt, void *inp) {
    raw_input *self = static_cast<raw_input *>(inp);

    self->pressed.emplace(evt->code);

    //std::cout << "Key down: " << evt->code << std::endl;
    return EM_TRUE;
  }

  EM_BOOL raw_input::keyup(int eventType, const EmscriptenKeyboardEvent *evt, void *inp) {
    raw_input *self = static_cast<raw_input *>(inp);

    self->pressed.erase(evt->code);

    //std::cout << "Key up: " << evt->code << std::endl;
    return EM_TRUE;
  }

  EM_BOOL raw_input::mousemove(int eventType, const EmscriptenMouseEvent *evt, void *inp) {
    raw_input *self = static_cast<raw_input *>(inp);

    self->mouse_motion += glm::vec2(evt->movementX, evt->movementY);

    //std::cout << "Mouse move: " << evt->movementX << ", " << evt->movementY << std::endl;
    return EM_TRUE;
  }


  EM_BOOL raw_input::pointerlock_change(int eventType, const EmscriptenPointerlockChangeEvent *evt, void *inp) {
    raw_input *self = static_cast<raw_input *>(inp);

    if(evt->isActive == EM_FALSE) {
      emscripten_request_pointerlock("canvas", EM_TRUE);
    }

    return EM_TRUE;
  }

  EM_BOOL raw_input::gamepad_connected(int eventType, const EmscriptenGamepadEvent *evt, void *inp) {
    raw_input *self = static_cast<raw_input *>(inp);

    self->gamepads_connected[evt->index] = *evt;

    std::cout << "Gamepad connected, count now: " << self->gamepads_connected.size() << std::endl;

    return EM_TRUE;
  }

  EM_BOOL raw_input::gamepad_disconnected(int eventType, const EmscriptenGamepadEvent *evt, void *inp) {
    raw_input *self = static_cast<raw_input *>(inp);

    self->gamepads_connected.erase(evt->index);

    std::cout << "Gamepad disconnected, count now: " << self->gamepads_connected.size() << std::endl;

    return EM_TRUE;
  }

  raw_input *raw_input::instance = nullptr;

}

