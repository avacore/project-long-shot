//
// Created by sbg on 7/29/17.
//

#ifndef LONGSHOT_CHUCK_H
#define LONGSHOT_CHUCK_H

#include <cstddef>
#include <cstdint>
#include <array>
#include <glm/vec3.hpp>
#include <unordered_set>

typedef uint32_t block_t;

class chunk {
 public:
  static constexpr int size = 16;

  chunk(int x, int y, int z);

  void generate();

  block_t get(size_t x, size_t y, size_t z) const;
  glm::vec3 get_relative_position(const glm::ivec3 &center_chunk) const;
  inline glm::ivec3 get_chunk_position() const { return glm::ivec3(x, y, z); };
  inline size_t get_version() const { return this->version; };

 private:
  void set(block_t block, size_t x, size_t y, size_t z);
  void kill_floater(std::unordered_set<glm::ivec3> &seen, size_t x, size_t y, size_t z);

  int x, y, z;
  size_t version;

  std::array<block_t, size*size*size> blocks;
};

#endif //LONGSHOT_CHUCK_H
