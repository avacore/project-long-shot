//
// Created by sbg on 8/26/17.
//

#ifndef LONGSHOT_HASHES_H
#define LONGSHOT_HASHES_H

#include <glm/vec3.hpp>

namespace std {
  template<>
  struct hash<glm::ivec3> {
    inline size_t operator()(const glm::ivec3 &vec) const {
      return vec.x ^ vec.y ^ vec.z;
    }
  };
}

#endif //LONGSHOT_HASHES_H
