//
// Created by sbg on 7/22/17.
//

#include "camera.h"

#include <glm/gtc/matrix_transform.hpp>

#include "input.h"
#include "utility.h"

camera::camera() :
    pitch("Pitch", {input::axis::MouseY, input::axis::Joy4}),
    yaw("Yaw", {input::axis::MouseX, input::axis::Joy3}),
    forward_backward("Forward/Backward", {input::axis::Joy1}),
    left_right("Left/Right", {input::axis::Joy0}),
    forward("Forward", {input::button::KeyW}),
    backward("Backward", {input::button::KeyS}),
    left("Strafe Left", {input::button::KeyA}),
    right("Strafe Right", {input::button::KeyD}),
    up("Jump", {input::button::KeySpace, input::button::Joy0}),
    down("Down", {input::button::KeyShiftLeft, input::button::KeyShiftRight, input::button::Joy1}) {
  this->projection_transform_data = glm::perspective(glm::pi<float>() / 3.f, 16.f / 9.f, .1f, 128.f);
  //this->position = glm::vec3(0.f, 0.f, 0.f);
  this->rotation = glm::vec3(0.f, 0.f, 0.f);
}

void camera::viewport_resize(size_t width, size_t height) {
  this->projection_transform_data = glm::perspective(glm::pi<float>() / 3.f, float(width) / float(height), .1f, 128.f);
}

glm::mat4 camera::projection_transform() const {
  return this->projection_transform_data;
}

glm::mat4 camera::view_transform() const {
  glm::mat4 transform(1.f);
  transform = glm::rotate(transform, this->rotation.x, glm::vec3(1.f, 0.f, 0.f));
  transform = glm::rotate(transform, this->rotation.y, glm::vec3(0.f, 1.f, 0.f));
  transform = glm::rotate(transform, this->rotation.z, glm::vec3(0.f, 0.f, 1.f));
  transform = glm::translate(transform, -this->position.get_local_postion());
  return transform;
}

void camera::tick(float time, float delta) {
  this->rotation.y += 0.1 * yaw.get();
  this->rotation.x += 0.1 * pitch.get();

  this->rotation.x = clamp(this->rotation.x, -glm::pi<float>() / 2, glm::pi<float>() / 2);

  if(this->rotation.y > glm::pi<float>() * 2) {
    this->rotation.y -= glm::pi<float>() * 2;
  }

  if(this->rotation.y < - glm::pi<float>() * 2) {
    this->rotation.y += glm::pi<float>() * 2;
  }

  glm::vec4 motion(0.f, 0.f, 0.f, 0.f);
  if(forward.get()) {
    motion.z -= 1;
  }
  if(backward.get()) {
    motion.z += 1;
  }
  if(left.get()) {
    motion.x -= 1;
  }
  if(right.get()) {
    motion.x += 1;
  }

  if(motion.z == 0) {
    motion.z = forward_backward.get();
  }
  if(motion.x == 0) {
    motion.x = left_right.get();
  }

  if(motion != glm::vec4{0.f, 0.f, 0.f, 0.f}) {
    glm::mat4 motion_transform(1.f);
    motion_transform = glm::rotate(motion_transform, -this->rotation.z, glm::vec3(0.f, 0.f, 1.f));
    motion_transform = glm::rotate(motion_transform, -this->rotation.y, glm::vec3(0.f, 1.f, 0.f));
    motion_transform = glm::rotate(motion_transform, -this->rotation.x, glm::vec3(1.f, 0.f, 0.f));

    motion = motion_transform*motion;
  }

  if(down.get()) {
    motion.y -= 1;
  }
  if(up.get()) {
    motion.y += 1;
  }

  if(motion != glm::vec4{0.f, 0.f, 0.f, 0.f}) {
    auto length = glm::length(motion);
    if(length > 1.f) {
      motion /= length;
    }
    motion *= delta * 0.01f;
    this->position += glm::vec3(motion.x, motion.y, motion.z);
  }
}