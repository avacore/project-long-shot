//
// Created by sbg on 7/22/17.
//

#ifndef LONGSHOT_INPUT_H
#define LONGSHOT_INPUT_H

#include <string>
#include <unordered_set>

#include <emscripten/html5.h>

#include <glm/vec2.hpp>
#include <vector>
#include <unordered_map>
#include <variant>

namespace input {
  class axis {
   public:
    struct axis_def {
      enum { Mouse, Joystick } type;
      size_t index;
    };

    axis(std::string name, std::vector<axis_def> defaults);

    float get();

    static constexpr axis_def MouseX = {axis_def::Mouse, 0};
    static constexpr axis_def MouseY = {axis_def::Mouse, 1};
    static constexpr axis_def Joy0 = {axis_def::Joystick, 0};
    static constexpr axis_def Joy1 = {axis_def::Joystick, 1};
    static constexpr axis_def Joy2 = {axis_def::Joystick, 2};
    static constexpr axis_def Joy3 = {axis_def::Joystick, 3};
    static constexpr axis_def Joy4 = {axis_def::Joystick, 4};

   private:
    std::vector<axis_def> controls;
  };

  class button {
   public:
    struct button_def {
      enum { Keyboard, Mouse, Joystick } type;
      union { size_t number; const char *name; } index;
    };

    button(std::string name, std::vector<button_def> defaults);

    bool get();

    static constexpr button_def Joy0 = {button_def::Joystick, {0}};
    static constexpr button_def Joy1 = {button_def::Joystick, {1}};
    static constexpr button_def Joy2 = {button_def::Joystick, {2}};
    static constexpr button_def Joy3 = {button_def::Joystick, {3}};
    static constexpr button_def Joy4 = {button_def::Joystick, {4}};

    static constexpr button_def KeyW = {button_def::Keyboard, {.name = "KeyW"}};
    static constexpr button_def KeyA = {button_def::Keyboard, {.name = "KeyA"}};
    static constexpr button_def KeyS = {button_def::Keyboard, {.name = "KeyS"}};
    static constexpr button_def KeyD = {button_def::Keyboard, {.name = "KeyD"}};
    static constexpr button_def KeySpace = {button_def::Keyboard, {.name = "Space"}};
    static constexpr button_def KeyShiftLeft = {button_def::Keyboard, {.name = "ShiftLeft"}};
    static constexpr button_def KeyShiftRight = {button_def::Keyboard, {.name = "ShiftRight"}};

   private:
    std::vector<button_def> keys;
  };

  class raw_input {
   public:
    static raw_input &get_instance();

    bool is_pressed(const std::string &key) const;
    glm::vec2 get_mouse_motion();
    float get_mouse_motion(size_t idx);
    float get_gamepad_axis(size_t idx);
    bool get_gamepad_button(size_t idx);

    void tick();

   private:
    raw_input();
    static raw_input *instance;

    std::unordered_set<std::string> pressed;
    glm::vec2 mouse_motion;
    float mouse_speed;

    std::unordered_map<size_t, EmscriptenGamepadEvent> gamepads_connected;

    static EM_BOOL keydown(int eventType, const EmscriptenKeyboardEvent *evt, void *inp);
    static EM_BOOL keyup(int eventType, const EmscriptenKeyboardEvent *evt, void *inp);
    static EM_BOOL mousemove(int eventType, const EmscriptenMouseEvent *evt, void *inp);
    static EM_BOOL pointerlock_change(int eventType, const EmscriptenPointerlockChangeEvent *evt, void *inp);
    static EM_BOOL gamepad_connected(int eventType, const EmscriptenGamepadEvent *evt, void *inp);
    static EM_BOOL gamepad_disconnected(int eventType, const EmscriptenGamepadEvent *evt, void *inp);
  };
}

#endif //LONGSHOT_INPUT_H
