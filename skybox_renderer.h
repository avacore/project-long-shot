//
// Created by sbg on 9/9/17.
//

#ifndef LONGSHOT_SKYBOX_RENDERER_H
#define LONGSHOT_SKYBOX_RENDERER_H

#include <string>
#include <array>
#include "gles_renderable.h"
#include "gles_material.h"

class skybox_renderer : public gles_renderable {
 public:
  skybox_renderer(const std::string &material);

  virtual void render(const glm::mat4 &current_model_transfrom, const glm::mat4 &current_view_transfrom, const glm::mat4 &current_projection_transfrom, const glm::ivec3 &center_chunk);

 private:
  gles_material material;
  static std::array<GLfloat, 4 * 8> vertex_buffer_data;
  static std::array<GLushort, 3 * 12> index_buffer_data;

  static void generate_buffers();
  static bool buffers_inited;
  static GLuint vertex_buffer;
  static GLuint index_buffer;
};

#endif //LONGSHOT_SKYBOX_RENDERER_H
