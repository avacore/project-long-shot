#include "gles_renderer.h"

#include <fstream>
#include <string>

#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>
#include <cmath>

#include "block.h"
#include "camera.h"
#include "texture.h"
#include "gles_material.h"

gles_renderer::gles_renderer() : mat("stone"), line_material("line"), width(100), height(100) {
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  glEnable(GL_CULL_FACE);
  glFrontFace(GL_CCW);
  glCullFace(GL_BACK);

  std::cout << "GL Error: " << glGetError() << std::endl;
}

gles_renderer::~gles_renderer() {

}

void gles_renderer::viewport_resize(size_t width, size_t height) {
  this->width = width;
  this->height = height;
}

void gles_renderer::render(const camera &cam, std::vector<gles_renderable *> render_list, float delta) {
  static glm::vec4 daylight_direction(0.f, 1.f, 0.f, 0.f);

  glViewport(0, 0, width, height);

  glm::mat4 model_mat = glm::mat4(1.0f);
  glm::mat4 view_mat = cam.view_transform();
  glm::mat4 projection_mat = cam.projection_transform();

  daylight_direction = glm::rotate(glm::mat4(1.f), 0.0001f * delta, glm::vec3(0.f, 0.f, 1.f)) * daylight_direction;

  glClearDepthf(1.f);
  glClearColor(0.f, 0.f, 0.f, 1.f);
  glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

  mat.activate();

  glUniformMatrix4fv(mat.get_uniform_projection_transfrom(), 1, GL_FALSE, glm::value_ptr(projection_mat));
  glUniformMatrix4fv(mat.get_uniform_view_transfrom(), 1, GL_FALSE, glm::value_ptr(view_mat));
  glUniform4fv(mat.get_uniform_daylight_direction(), 1, glm::value_ptr(daylight_direction));

  //block b;
  //b.render(vertex_loc);
  //std::cout << "GL Error: " << glGetError() << std::endl;
  for(auto *renderable : render_list) {
    renderable->render(model_mat, view_mat, projection_mat, cam.get_position().get_chunk_postion());
    //std::cout << "GL Error: " << glGetError() << std::endl;
  }
}

void gles_renderer::chunk_added(const chunk &chunk) {

}

void gles_renderer::chunk_removed(const chunk &chunk) {
  auto renderable_iter = this->chunk_renderers.find(&chunk);
  if(renderable_iter != this->chunk_renderers.end()) {
    this->chunk_renderers.erase(renderable_iter);
  }
}

gles_renderable* gles_renderer::create_chunk_renderer(const chunk &chunk) {
  auto renderable_iter = this->chunk_renderers.find(&chunk);
  if(renderable_iter == this->chunk_renderers.end()) {
    auto renderable_owner = std::make_unique<chunk_renderer>(chunk, mat, line_material);
    auto renderable = renderable_owner.get();
    this->chunk_renderers[&chunk] = std::move(renderable_owner);
    return renderable;
  } else {
    return renderable_iter->second.get();
  }
}

gles_renderable* gles_renderer::create_skybox_renderer(const std::string &material) {
  auto renderable_iter = this->skybox_renderers.find(material);
  if(renderable_iter == this->skybox_renderers.end()) {
    auto renderable_owner = std::make_unique<skybox_renderer>(material);
    auto renderable = renderable_owner.get();
    this->skybox_renderers[material] = std::move(renderable_owner);
    return renderable;
  } else {
    return renderable_iter->second.get();
  }
}