//
// Created by sbg on 7/15/17.
//

#include "egl_display.h"

#include <iostream>

egl_display::egl_display() {
  this->display = eglGetDisplay(0);

  EGLint major, minor;
  eglInitialize(this->display, &major, &minor);

  std::cout << "EGL Version: " << major << "." << minor << std::endl;

  EGLConfig configs[16];
  EGLint num_configs;
  eglGetConfigs(this->display, configs, sizeof(configs), &num_configs);

  std::cout << "EGL Configurations: " << num_configs << std::endl;
  for (EGLint config = 0; config < num_configs; ++config) {
    EGLint r, g, b;

    eglGetConfigAttrib(this->display, configs[config], EGL_RED_SIZE, &r);
    eglGetConfigAttrib(this->display, configs[config], EGL_GREEN_SIZE, &g);
    eglGetConfigAttrib(this->display, configs[config], EGL_BLUE_SIZE, &b);

    std::cout << "  EGL Config RGB: " << r << "/" << g << "/" << b << std::endl;
  }

  // TODO: Pick the config based on attributes.
  EGLConfig config = configs[0];

  // Create Native window here

  this->surface = eglCreateWindowSurface(this->display, config, 0, nullptr);
  EGLint context_attributes[] = {
      EGL_CONTEXT_CLIENT_VERSION, 2,
      EGL_NONE
  };
  this->context = eglCreateContext(this->display, config, nullptr, context_attributes);
}

egl_display::~egl_display() {

}

void egl_display::make_current() {
  eglMakeCurrent(this->display, this->surface, this->surface, this->context);
}
