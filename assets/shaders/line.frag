#version 300 es

in lowp vec4 varying_colour;

out lowp vec4 fragment_colour;

void main() {
    fragment_colour = varying_colour;
}
