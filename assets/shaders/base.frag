#version 300 es

uniform lowp sampler2DArray base;
uniform lowp vec4 daylight_direction;

in lowp vec2 varying_texture_coord;
in lowp vec4 varying_normal;
flat in uint varying_material;

out lowp vec4 fragment_colour;

const lowp vec4 ambient = vec4(0.3, 0.3, 0.3, 1.0);
const lowp vec4 daylight = vec4(1.0, 1.0, 1.0, 1.0);


void main() {
    fragment_colour = ambient;
    fragment_colour += max(dot(varying_normal, daylight_direction), 0.0) * daylight;
    fragment_colour *= texture(base, vec3(varying_texture_coord, varying_material));
}
