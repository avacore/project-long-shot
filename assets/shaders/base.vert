#version 300 es

uniform mat4 model_transform;
uniform mat4 view_transform;
uniform mat4 projection_transform;

in vec4 vertex;
in vec4 normal;
in vec2 texture;
in uint material;

out vec2 varying_texture_coord;
out vec4 varying_normal;
flat out uint varying_material;

void main() {
	gl_Position = projection_transform * view_transform * model_transform * vertex;
	varying_normal = normalize(model_transform * normal);

	varying_texture_coord = texture;

	varying_material = 0u;
	if(material == 1u) {
    	varying_material = 0u;
	} else if(material == 2u) {
        if(varying_normal.y > 0.5) {
            varying_material = 2u;
        } else {
            varying_material = 1u;
        }
    }
}
