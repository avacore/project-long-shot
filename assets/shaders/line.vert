#version 300 es

uniform mat4 model_transform;
uniform mat4 view_transform;
uniform mat4 projection_transform;

in vec4 vertex;
in vec4 colour;

out vec4 varying_colour;

void main() {
	gl_Position = projection_transform * view_transform * model_transform * vertex;
	varying_colour = colour;
}
