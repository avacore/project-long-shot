#version 100

uniform mat4 view_transform;
uniform mat4 projection_transform;

attribute vec4 vertex;

varying vec3 varying_texture_coord;

void main() {
	vec3 temp = (view_transform * vertex).xyz;
	//gl_Position.w = 1.0;
	gl_Position = projection_transform * vec4(temp, 1);
	gl_Position.z = gl_Position.w;

	varying_texture_coord = vertex.xyz;
}
