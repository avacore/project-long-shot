#version 100

uniform samplerCube sky;

varying lowp vec3 varying_texture_coord;

void main() {
    gl_FragColor = textureCube(sky, varying_texture_coord);
}
