//
// Created by sbg on 7/29/17.
//

#include "chunk_renderer.h"
#include "gles_material.h"

#include <iostream>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

chunk_renderer::chunk_renderer(const class chunk &chunk, const gles_material &material, const gles_material &line_material) : chunk(chunk), material(material), line_material(line_material) {
  glGenBuffers(1, &vertex_buffer);
  glGenBuffers(1, &line_buffer);

  generate_buffers();
}

void chunk_renderer::generate_buffers() {
  for (size_t x = 0; x < chunk::size; ++x) {
    for (size_t y = 0; y < chunk::size; ++y) {
      for (size_t z = 0; z < chunk::size; ++z) {
        auto material = this->chunk.get(x, y, z);
        if(material != 0) {
          if(x == chunk::size - 1 || this->chunk.get(x + 1, y, z) == 0) {
            add_face_x_positive(x, y, z, material);
          }
          if(x == 0 || this->chunk.get(x - 1, y, z) == 0) {
            add_face_x_negative(x, y, z, material);
          }
          if(y == chunk::size - 1 || this->chunk.get(x, y + 1, z) == 0) {
            add_face_y_positive(x, y, z, material);
          }
          if(y == 0 || this->chunk.get(x, y - 1, z) == 0) {
            add_face_y_negative(x, y, z, material);
          }
          if(z == chunk::size - 1 || this->chunk.get(x, y, z + 1) == 0) {
            add_face_z_positive(x, y, z, material);
          }
          if(z == 0 || this->chunk.get(x, y, z - 1) == 0) {
            add_face_z_negative(x, y, z, material);
          }
        }
      }
    }
  }

  line_buffer_data.push_back({-8.f,  8.f, -8.f, 1.f, 1.f, 0.f, 0.f, 1.f});
  line_buffer_data.push_back({ 8.f,  8.f, -8.f, 1.f, 1.f, 0.f, 0.f, 1.f});
  line_buffer_data.push_back({-8.f,  8.f, -8.f, 1.f, 1.f, 0.f, 0.f, 1.f});
  line_buffer_data.push_back({-8.f, -8.f, -8.f, 1.f, 1.f, 0.f, 0.f, 1.f});
  line_buffer_data.push_back({-8.f,  8.f, -8.f, 1.f, 1.f, 0.f, 0.f, 1.f});
  line_buffer_data.push_back({-8.f,  8.f,  8.f, 1.f, 1.f, 0.f, 0.f, 1.f});

  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
  glBufferData(GL_ARRAY_BUFFER, vertex_buffer_data.size() * sizeof(local_vertex_data), vertex_buffer_data.data(), GL_STATIC_DRAW);

  glBindBuffer(GL_ARRAY_BUFFER, line_buffer);
  glBufferData(GL_ARRAY_BUFFER, line_buffer_data.size() * sizeof(line_vertex_data), line_buffer_data.data(), GL_STATIC_DRAW);

  //std::cout << "Vertex: " << vertex_buffer_data.size() << std::endl;
  //std::cout << "Index:  " << index_buffer_data.size() << std::endl;

  chunk_version = chunk.get_version();
}

void chunk_renderer::render(const glm::mat4 &current_model_transfrom, const glm::mat4 &current_view_transfrom, const glm::mat4 &current_projection_transfrom, const glm::ivec3 &center_chunk) {
  if(chunk_version != chunk.get_version()) {
    //generate_local_vertex_buffer();
    //generate_index_buffer();
    generate_buffers();
  }

  if(vertex_buffer_data.size() > 0) {
    material.activate();
    auto model_transform = material.get_uniform_model_transfrom();
    auto vertex_attrib = material.get_attribute_vertex_position();
    auto texture_coord_attrib = material.get_attribute_vertex_texture();
    auto normal_attrib = material.get_attribute_vertex_normal();
    auto material_attrib = material.get_attribute_material();

    glm::mat4 local_model_transfrom = glm::translate(current_model_transfrom, chunk.get_relative_position(center_chunk));
    glUniformMatrix4fv(model_transform, 1, GL_FALSE, glm::value_ptr(local_model_transfrom));

    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glVertexAttribPointer(vertex_attrib, 4, GL_FLOAT, GL_FALSE, sizeof(local_vertex_data), reinterpret_cast<void *>(local_vertex_data::offset_vertex_coords));
    glEnableVertexAttribArray(vertex_attrib);

    glVertexAttribPointer(texture_coord_attrib, 2, GL_FLOAT, GL_FALSE, sizeof(local_vertex_data), reinterpret_cast<void *>(local_vertex_data::offset_texture_coords));
    glEnableVertexAttribArray(texture_coord_attrib);

    glVertexAttribPointer(normal_attrib, 4, GL_FLOAT, GL_FALSE, sizeof(local_vertex_data), reinterpret_cast<void *>(local_vertex_data::offset_normal_coords));
    glEnableVertexAttribArray(normal_attrib);

    glVertexAttribIPointer(material_attrib, 1, GL_UNSIGNED_INT, sizeof(local_vertex_data), reinterpret_cast<void *>(local_vertex_data::offset_material));
    glEnableVertexAttribArray(material_attrib);

    glDrawArrays(GL_TRIANGLES, 0, vertex_buffer_data.size());
  }

  if(false) {
    line_material.activate();
    auto vertex_attrib = line_material.get_attribute_vertex_position();
    auto colour_attrib = line_material.get_attribute_vertex_colour();

    glLineWidth(3.f);

    glm::mat4 local_model_transfrom = glm::translate(current_model_transfrom, chunk.get_relative_position(center_chunk));
    glUniformMatrix4fv(line_material.get_uniform_model_transfrom(), 1, GL_FALSE, glm::value_ptr(local_model_transfrom));

    glUniformMatrix4fv(line_material.get_uniform_view_transfrom(), 1, GL_FALSE, glm::value_ptr(current_view_transfrom));
    glUniformMatrix4fv(line_material.get_uniform_projection_transfrom(), 1, GL_FALSE, glm::value_ptr(current_projection_transfrom));

    glBindBuffer(GL_ARRAY_BUFFER, line_buffer);
    glVertexAttribPointer(vertex_attrib, 4, GL_FLOAT, GL_FALSE, sizeof(line_vertex_data), reinterpret_cast<void *>(line_vertex_data::offset_vertex_coords));
    glEnableVertexAttribArray(vertex_attrib);

    glVertexAttribPointer(colour_attrib, 4, GL_FLOAT, GL_FALSE, sizeof(line_vertex_data), reinterpret_cast<void *>(line_vertex_data::offset_vertex_colours));
    glEnableVertexAttribArray(colour_attrib);

    glDrawArrays(GL_LINES, 0, line_buffer_data.size());
  }

}

void chunk_renderer::add_face_x_positive(size_t x, size_t y, size_t z, block_t material) {
  vertex_buffer_data.push_back({x + 1.f - chunk::size/2.0f, y + 0.f - chunk::size/2.0f, z + 0.f - chunk::size/2.0f, 1.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 1.f - chunk::size/2.0f, y + 1.f - chunk::size/2.0f, z + 0.f - chunk::size/2.0f, 1.f, 1.f, 0.f, 1.f, 0.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 1.f - chunk::size/2.0f, y + 0.f - chunk::size/2.0f, z + 1.f - chunk::size/2.0f, 1.f, 0.f, 1.f, 1.f, 0.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 1.f - chunk::size/2.0f, y + 0.f - chunk::size/2.0f, z + 1.f - chunk::size/2.0f, 1.f, 0.f, 1.f, 1.f, 0.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 1.f - chunk::size/2.0f, y + 1.f - chunk::size/2.0f, z + 0.f - chunk::size/2.0f, 1.f, 1.f, 0.f, 1.f, 0.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 1.f - chunk::size/2.0f, y + 1.f - chunk::size/2.0f, z + 1.f - chunk::size/2.0f, 1.f, 1.f, 1.f, 1.f, 0.f, 0.f, 0.f, material});
}

void chunk_renderer::add_face_x_negative(size_t x, size_t y, size_t z, block_t material) {
  vertex_buffer_data.push_back({x + 0.f - chunk::size/2.0f, y + 0.f - chunk::size/2.0f, z + 0.f - chunk::size/2.0f, 1.f, 0.f, 0.f, -1.f, 0.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 0.f - chunk::size/2.0f, y + 0.f - chunk::size/2.0f, z + 1.f - chunk::size/2.0f, 1.f, 0.f, 1.f, -1.f, 0.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 0.f - chunk::size/2.0f, y + 1.f - chunk::size/2.0f, z + 0.f - chunk::size/2.0f, 1.f, 1.f, 0.f, -1.f, 0.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 0.f - chunk::size/2.0f, y + 1.f - chunk::size/2.0f, z + 0.f - chunk::size/2.0f, 1.f, 1.f, 0.f, -1.f, 0.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 0.f - chunk::size/2.0f, y + 0.f - chunk::size/2.0f, z + 1.f - chunk::size/2.0f, 1.f, 0.f, 1.f, -1.f, 0.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 0.f - chunk::size/2.0f, y + 1.f - chunk::size/2.0f, z + 1.f - chunk::size/2.0f, 1.f, 1.f, 1.f, -1.f, 0.f, 0.f, 0.f, material});
}

void chunk_renderer::add_face_y_positive(size_t x, size_t y, size_t z, block_t material) {
  vertex_buffer_data.push_back({x + 0.f - chunk::size/2.0f, y + 1.f - chunk::size/2.0f, z + 0.f - chunk::size/2.0f, 1.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 0.f - chunk::size/2.0f, y + 1.f - chunk::size/2.0f, z + 1.f - chunk::size/2.0f, 1.f, 0.f, 1.f, 0.f, 1.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 1.f - chunk::size/2.0f, y + 1.f - chunk::size/2.0f, z + 0.f - chunk::size/2.0f, 1.f, 1.f, 0.f, 0.f, 1.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 1.f - chunk::size/2.0f, y + 1.f - chunk::size/2.0f, z + 0.f - chunk::size/2.0f, 1.f, 1.f, 0.f, 0.f, 1.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 0.f - chunk::size/2.0f, y + 1.f - chunk::size/2.0f, z + 1.f - chunk::size/2.0f, 1.f, 0.f, 1.f, 0.f, 1.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 1.f - chunk::size/2.0f, y + 1.f - chunk::size/2.0f, z + 1.f - chunk::size/2.0f, 1.f, 1.f, 1.f, 0.f, 1.f, 0.f, 0.f, material});
}

void chunk_renderer::add_face_y_negative(size_t x, size_t y, size_t z, block_t material) {
  vertex_buffer_data.push_back({x + 0.f - chunk::size/2.0f, y + 0.f - chunk::size/2.0f, z + 0.f - chunk::size/2.0f, 1.f, 0.f, 0.f, 0.f, -1.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 1.f - chunk::size/2.0f, y + 0.f - chunk::size/2.0f, z + 0.f - chunk::size/2.0f, 1.f, 1.f, 0.f, 0.f, -1.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 0.f - chunk::size/2.0f, y + 0.f - chunk::size/2.0f, z + 1.f - chunk::size/2.0f, 1.f, 0.f, 1.f, 0.f, -1.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 0.f - chunk::size/2.0f, y + 0.f - chunk::size/2.0f, z + 1.f - chunk::size/2.0f, 1.f, 0.f, 1.f, 0.f, -1.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 1.f - chunk::size/2.0f, y + 0.f - chunk::size/2.0f, z + 0.f - chunk::size/2.0f, 1.f, 1.f, 0.f, 0.f, -1.f, 0.f, 0.f, material});
  vertex_buffer_data.push_back({x + 1.f - chunk::size/2.0f, y + 0.f - chunk::size/2.0f, z + 1.f - chunk::size/2.0f, 1.f, 1.f, 1.f, 0.f, -1.f, 0.f, 0.f, material});
}

void chunk_renderer::add_face_z_positive(size_t x, size_t y, size_t z, block_t material) {
  vertex_buffer_data.push_back({x + 0.f - chunk::size/2.0f, y + 0.f - chunk::size/2.0f, z + 1.f - chunk::size/2.0f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, material});
  vertex_buffer_data.push_back({x + 1.f - chunk::size/2.0f, y + 0.f - chunk::size/2.0f, z + 1.f - chunk::size/2.0f, 1.f, 1.f, 0.f, 0.f, 0.f, 1.f, 0.f, material});
  vertex_buffer_data.push_back({x + 0.f - chunk::size/2.0f, y + 1.f - chunk::size/2.0f, z + 1.f - chunk::size/2.0f, 1.f, 0.f, 1.f, 0.f, 0.f, 1.f, 0.f, material});
  vertex_buffer_data.push_back({x + 0.f - chunk::size/2.0f, y + 1.f - chunk::size/2.0f, z + 1.f - chunk::size/2.0f, 1.f, 0.f, 1.f, 0.f, 0.f, 1.f, 0.f, material});
  vertex_buffer_data.push_back({x + 1.f - chunk::size/2.0f, y + 0.f - chunk::size/2.0f, z + 1.f - chunk::size/2.0f, 1.f, 1.f, 0.f, 0.f, 0.f, 1.f, 0.f, material});
  vertex_buffer_data.push_back({x + 1.f - chunk::size/2.0f, y + 1.f - chunk::size/2.0f, z + 1.f - chunk::size/2.0f, 1.f, 1.f, 1.f, 0.f, 0.f, 1.f, 0.f, material});
}

void chunk_renderer::add_face_z_negative(size_t x, size_t y, size_t z, block_t material) {
  vertex_buffer_data.push_back({x + 0.f - chunk::size/2.0f, y + 0.f - chunk::size/2.0f, z + 0.f - chunk::size/2.0f, 1.f, 0.f, 0.f, 0.f, 0.f, -1.f, 0.f, material});
  vertex_buffer_data.push_back({x + 0.f - chunk::size/2.0f, y + 1.f - chunk::size/2.0f, z + 0.f - chunk::size/2.0f, 1.f, 0.f, 1.f, 0.f, 0.f, -1.f, 0.f, material});
  vertex_buffer_data.push_back({x + 1.f - chunk::size/2.0f, y + 0.f - chunk::size/2.0f, z + 0.f - chunk::size/2.0f, 1.f, 1.f, 0.f, 0.f, 0.f, -1.f, 0.f, material});
  vertex_buffer_data.push_back({x + 1.f - chunk::size/2.0f, y + 0.f - chunk::size/2.0f, z + 0.f - chunk::size/2.0f, 1.f, 1.f, 0.f, 0.f, 0.f, -1.f, 0.f, material});
  vertex_buffer_data.push_back({x + 0.f - chunk::size/2.0f, y + 1.f - chunk::size/2.0f, z + 0.f - chunk::size/2.0f, 1.f, 0.f, 1.f, 0.f, 0.f, -1.f, 0.f, material});
  vertex_buffer_data.push_back({x + 1.f - chunk::size/2.0f, y + 1.f - chunk::size/2.0f, z + 0.f - chunk::size/2.0f, 1.f, 1.f, 1.f, 0.f, 0.f, -1.f, 0.f, material});
}

const size_t chunk_renderer::local_vertex_data::offset_vertex_coords = offsetof(chunk_renderer::local_vertex_data, x);
const size_t chunk_renderer::local_vertex_data::offset_texture_coords = offsetof(chunk_renderer::local_vertex_data, s);
const size_t chunk_renderer::local_vertex_data::offset_normal_coords = offsetof(chunk_renderer::local_vertex_data, nx);
const size_t chunk_renderer::local_vertex_data::offset_material = offsetof(chunk_renderer::local_vertex_data, material);

const size_t chunk_renderer::line_vertex_data::offset_vertex_coords = offsetof(chunk_renderer::line_vertex_data, x);
const size_t chunk_renderer::line_vertex_data::offset_vertex_colours = offsetof(chunk_renderer::line_vertex_data, r);
