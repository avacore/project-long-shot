#include <iostream>

#include <memory>
#include <emscripten.h>

#include "camera.h"
#include "egl_display.h"
#include "gles_renderer.h"
#include "input.h"
#include "chunk.h"
#include "chunk_renderer.h"
#include "texture.h"
#include "world.h"

std::unique_ptr<egl_display> display;
std::unique_ptr<gles_renderer> renderer;
std::unique_ptr<camera> cam;
std::unique_ptr<world> w;

float last_tick;
float first_tick;

void mainloop() {
  float now = emscripten_get_now();
  float delta = now - last_tick;

  input::raw_input::get_instance().tick();

  cam->tick(now - first_tick, delta);

  const auto &cs = w->get_close_chucks(cam->get_position());

  std::vector<gles_renderable *> render_list;
  render_list.reserve(cs.size());
  for(const auto &c : cs) {
    render_list.push_back(renderer->create_chunk_renderer(*c));
  }

  render_list.push_back(renderer->create_skybox_renderer("skybox"));

  display->make_current();
  renderer->render(*cam, render_list, delta);

  last_tick = now;
}

EM_BOOL on_resize(int event_Type, const EmscriptenUiEvent *ui_event, void *userData) {
  double width, height;
  emscripten_get_element_css_size(0, &width, &height);

  emscripten_set_canvas_size(width, height);
  renderer->viewport_resize(width, height);
  cam->viewport_resize(width, height);

  return EM_TRUE;
}

int main() {
  std::cout << "Hello, World!" << std::endl;
  std::cout << "From project Long Shot!" << std::endl;

  display = std::make_unique<egl_display>();
  display->make_current();

  std::cout << "GL Version: " << glGetString(GL_VERSION) << std::endl;

  renderer = std::make_unique<gles_renderer>();

  cam = std::make_unique<camera>();

  on_resize(0, nullptr, nullptr);
  emscripten_set_resize_callback("#window", nullptr, true, on_resize);
  emscripten_set_resize_callback("#canvas", nullptr, true, on_resize);

  w = std::make_unique<world>();
  w->chunk_listener_add(renderer.get());

  last_tick = first_tick = emscripten_get_now();
  emscripten_set_main_loop(mainloop, 0, false);

  return 0;
}
