//
// Created by sbg on 7/15/17.
//

#ifndef LONGSHOT_GLES_RENDERER_H
#define LONGSHOT_GLES_RENDERER_H

#include <string>
#include <unordered_map>
#include <vector>

#include <GLES2/gl2.h>
#include <map>

#include "chunk_renderer.h"
#include "gles_material.h"
#include "hashes.h"
#include "world.h"
#include "skybox_renderer.h"

class camera;
class chunk;
class gles_renderable;

class gles_renderer : public chunk_listener {
 public:
  gles_renderer();
  ~gles_renderer();

  void viewport_resize(size_t width, size_t height);
  void render(const camera &cam, std::vector<gles_renderable *> render_list, float delta);

  virtual void chunk_added(const chunk &chunk);
  virtual void chunk_removed(const chunk &chunk);
  gles_renderable *create_chunk_renderer(const chunk &chunk);
  gles_renderable *create_skybox_renderer(const std::string &material);


 private:
  gles_material mat;
  gles_material line_material;
  size_t width, height;

  std::unordered_map<const chunk *, std::unique_ptr<chunk_renderer>> chunk_renderers;
  std::map<const std::string, std::unique_ptr<skybox_renderer>> skybox_renderers;
};

#endif //LONGSHOT_GLES_RENDERER_H
