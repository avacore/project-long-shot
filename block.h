//
// Created by sbg on 7/22/17.
//

#ifndef LONGSHOT_BLOCK_H
#define LONGSHOT_BLOCK_H

#include <GLES2/gl2.h>

class block {
 public:
  block();

  void render(GLint vertex_attrib) const;

 private:
  static GLfloat vertexes[];
  static GLushort indicies[];

  GLuint vbuf, ibuf;
};

#endif //LONGSHOT_BLOCK_H
