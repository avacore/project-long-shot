//
// Created by sbg on 7/22/17.
//

#ifndef LONGSHOT_CAMERA_H
#define LONGSHOT_CAMERA_H

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include "absolute_position.h"
#include "input.h"

class camera {
 public:
  camera();

  void viewport_resize(size_t width, size_t height);
  inline absolute_position get_position() const { return this->position; }

  glm::mat4 projection_transform() const;
  glm::mat4 view_transform() const;
  void tick(float time, float delta);

 private:
  absolute_position position;
  glm::vec3 rotation;
  glm::mat4 projection_transform_data;

  input::axis pitch, yaw, forward_backward, left_right;
  input::button forward, backward, left, right, up, down;
};

#endif //LONGSHOT_CAMERA_H
