#ifndef LONGSHOT_EGL_DISPLAY_H
#define LONGSHOT_EGL_DISPLAY_H

#include <EGL/egl.h>

class egl_display {
 public:
  egl_display();
  ~egl_display();

  void make_current();

 private:
  EGLDisplay display;
  EGLSurface surface;
  EGLContext context;
};

#endif //LONGSHOT_EGL_DISPLAY_H
