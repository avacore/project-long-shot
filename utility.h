//
// Created by sbg on 10/7/17.
//

#ifndef LONGSHOT_UTILITY_H
#define LONGSHOT_UTILITY_H

#include <algorithm>

template<typename T>
T clamp(T value, T min, T max) {
  return std::min(std::max(value, min), max);
}

#endif //LONGSHOT_UTILITY_H
