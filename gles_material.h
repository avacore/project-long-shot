//
// Created by sbg on 8/5/17.
//

#ifndef LONGSHOT_GLES_MATERIAL_H
#define LONGSHOT_GLES_MATERIAL_H

#include <string>
#include <unordered_map>
#include <GLES3/gl3.h>
#include "texture.h"

class gles_material {
 public:
  gles_material(std::string name);

  void activate() const;

  GLint get_uniform_model_transfrom() const;
  GLint get_uniform_view_transfrom() const;
  GLint get_uniform_projection_transfrom() const;
  GLint get_uniform_daylight_direction() const;

  GLint get_attribute_vertex_position() const;
  GLint get_attribute_vertex_normal() const;
  GLint get_attribute_vertex_texture() const;
  GLint get_attribute_vertex_colour() const;
  GLint get_attribute_material() const;

 private:
  GLuint load_shader(GLenum type, const std::string &file);

  GLuint load_texture_2d(const std::string &file);
  GLuint load_texture_2d_array(const std::vector<std::string> &file);
  GLuint load_texture_cubemap(const std::string &top, const std::string &bottom, const std::string &front, const std::string &back, const std::string &left, const std::string &right);

  std::unordered_map<std::string, std::tuple<GLint, int, GLint, GLenum, std::unique_ptr<texture>>> textures;
  GLuint shader_program;
  GLuint shader_vertex;
  GLuint shader_fragment;

  mutable GLint uniform_model_transfrom, uniform_view_transfrom, uniform_projection_transfrom, uniform_daylight_direction, attribute_vertex_position, attribute_vertex_normal, attribute_vertex_texture, attribute_vertex_colour, attribute_material;
};

#endif //LONGSHOT_GLES_MATERIAL_H
