//
// Created by sbg on 8/26/17.
//

#ifndef LONGSHOT_WORLD_H
#define LONGSHOT_WORLD_H

#include <vector>
#include <set>
#include <unordered_set>
#include "absolute_position.h"
#include "chunk.h"
#include "hashes.h"

class chunk_listener {
 public:
  virtual void chunk_added(const chunk &) = 0;
  virtual void chunk_removed(const chunk &) = 0;
};

class world {
 public:
  world();

  const std::vector<std::unique_ptr<chunk>> &get_close_chucks(const absolute_position &center);

  void chunk_listener_add(chunk_listener *listener);

 private:
  static constexpr int radius = 8;
  static constexpr int radius_sqr = radius * radius;
  std::unordered_set<glm::ivec3> view_sphere;

  std::vector<std::unique_ptr<chunk>> chunks;

  std::vector<chunk_listener *> chunk_listeners;
};

#endif //LONGSHOT_WORLD_H
