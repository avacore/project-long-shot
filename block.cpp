//
// Created by sbg on 7/22/17.
//

#include "block.h"

GLfloat block::vertexes[] = {
     1.f,  1.f,  1.f, 1.f,
     1.f,  1.f, -1.f, 1.f,
     1.f, -1.f,  1.f, 1.f,
     1.f, -1.f, -1.f, 1.f,
    -1.f,  1.f,  1.f, 1.f,
    -1.f,  1.f, -1.f, 1.f,
    -1.f, -1.f,  1.f, 1.f,
    -1.f, -1.f, -1.f, 1.f,
};

GLushort block::indicies[] = {
    0, 1, 2,
    1, 2, 3,
    4, 5, 6,
    5, 6, 7,
    0, 1, 4,
    1, 4, 5,
    2, 3, 6,
    3, 6, 7,
    0, 2, 4,
    2, 4, 6,
    1, 3, 5,
    3, 5, 7,
};

block::block() {
  glGenBuffers(1, &this->vbuf);
  glBindBuffer(GL_ARRAY_BUFFER, this->vbuf);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertexes)*sizeof(GLfloat), vertexes, GL_STATIC_DRAW);

  glGenBuffers(1, &this->ibuf);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->ibuf);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indicies)*sizeof(GLushort), indicies, GL_STATIC_DRAW);
}

void block::render(GLint vertex_attrib) const {
  glBindBuffer(GL_ARRAY_BUFFER, this->vbuf);
  glVertexAttribPointer(vertex_attrib, 4, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*4, 0);
  glEnableVertexAttribArray(vertex_attrib);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->ibuf);
  glDrawElements(GL_TRIANGLES, sizeof(indicies), GL_UNSIGNED_SHORT, 0);
}

#if 0
GLfloat verts[] = {
      -0.5f, -0.5f, 0, 1,
      0.5f, -0.5f, 0, 1,
      -0.5f, 0.5f, 0, 1,
      0.5f, 0.5f, 0, 1,
  };

  GLushort elems[] = {0, 1, 2, 1, 2, 3};


  GLuint buf;
  glGenBuffers(1, &buf);
  glBindBuffer(GL_ARRAY_BUFFER, buf);
  glBufferData(GL_ARRAY_BUFFER, sizeof(verts)*sizeof(GLfloat), verts, GL_STATIC_DRAW);

  GLuint ebuf;
  glGenBuffers(1, &ebuf);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebuf);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elems)*sizeof(GLushort), elems, GL_STATIC_DRAW);

  glVertexAttribPointer(vertex_loc, 4, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*4, 0);
  glEnableVertexAttribArray(vertex_loc);

  glDrawElements(GL_TRIANGLES, sizeof(elems), GL_UNSIGNED_SHORT, 0);
#endif